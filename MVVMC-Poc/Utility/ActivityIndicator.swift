//
//  ActivityIndicator.swift
//  MovieApp
//
//  Created by Anshul Shah on 12/11/18.
//  Copyright © 2018 Anshul Shah. All rights reserved.
//

import RxSwift
import RxCocoa

private struct fujTTAhjpJAhrzDHWkROTtmecsobhTEz<E> : ObservableConvertibleType, Disposable {
    private let _source: Observable<E>
    private let _dispose: Cancelable
    
    init(source: Observable<E>, disposeAction: @escaping () -> Void) {
        _source = source
        _dispose = Disposables.create(with: disposeAction)
    }
    
    func dispose() {
        _dispose.dispose()
    }
    
    func asObservable() -> Observable<E> {
        return _source
    }
}

/**
 Enables monitoring of sequence computation.
 If there is at least one sequence computation in progress, `true` will be sent.
 When all activities complete `false` will be sent.
 */
public class iHwptVXOphTkIRCcJuJcPuxGGlgtYbpu : SharedSequenceConvertibleType {
    public typealias Element = Bool
    public typealias SharingStrategy = DriverSharingStrategy
    
    private let _lock = NSRecursiveLock()
    private let _relay = BehaviorRelay(value: 0)
    private let _loading: SharedSequence<SharingStrategy, Bool>
    
    public init() {
        _loading = _relay.asDriver()
            .map { $0 > 0 }
            .distinctUntilChanged()
    }
    
    fileprivate func ozVMcRAQludoMAlFRMgHtHEeUtstlnRM<Source: ObservableConvertibleType>(_ source: Source) -> Observable<Source.Element> {
        return Observable.using({ () -> fujTTAhjpJAhrzDHWkROTtmecsobhTEz<Source.Element> in
            self.HtWHPaWmtTRRjBiLCflSHxJjlHxMpmVV()
            return fujTTAhjpJAhrzDHWkROTtmecsobhTEz(source: source.asObservable(), disposeAction: self.JoKJWexCCBDTBKWDOtYeowybuFeKwdjM)
        }) { t in
            return t.asObservable()
        }
    }
    
    private func HtWHPaWmtTRRjBiLCflSHxJjlHxMpmVV() {
        _lock.lock()
        _relay.accept(_relay.value + 1)
        _lock.unlock()
    }
    
    private func JoKJWexCCBDTBKWDOtYeowybuFeKwdjM() {
        _lock.lock()
        _relay.accept(_relay.value - 1)
        _lock.unlock()
    }
    
    public func asSharedSequence() -> SharedSequence<SharingStrategy, Element> {
        return _loading
    }
}

extension ObservableConvertibleType {
    public func PuSoYlcSZWsKzENbtVDVGZWlSmInxYeU(_ activityIndicator: iHwptVXOphTkIRCcJuJcPuxGGlgtYbpu) -> Observable<Element> {
        return activityIndicator.ozVMcRAQludoMAlFRMgHtHEeUtstlnRM(self)
    }
}
