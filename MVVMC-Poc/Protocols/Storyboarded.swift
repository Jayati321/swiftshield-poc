//
//  Storyboarded.swift
//  MVVMC-Poc
//
//  Created by SUNNY on 09/04/20.
//  Copyright © 2020 SUNNY. All rights reserved.
//

import Foundation
import UIKit

protocol rnCAWUnHLFrDJJnnrHYtHmqlBYgJKHau {
    static func dJLzEXVoeuZIiampXDUjbJkzYaiYUSEL(identifierName: String) -> Self
}

extension rnCAWUnHLFrDJJnnrHYtHmqlBYgJKHau where Self: UIViewController {
    static func dJLzEXVoeuZIiampXDUjbJkzYaiYUSEL(identifierName: String) -> Self {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(identifier: identifierName) as! Self
    }
}
