//
//  AppCoordinator.swift
//  MVVMC-Poc
//
//  Created by SUNNY on 09/04/20.
//  Copyright © 2020 SUNNY. All rights reserved.
//

import Foundation
import UIKit

class saMhvIuXcEUWfBuLcvUnKlUpxsfwkmgc: edtcilqUaIhCpEmLaXuQwDmcnYvTmQoB {
    var childCoordinators = [edtcilqUaIhCpEmLaXuQwDmcnYvTmQoB]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func ENLrvGDdGqBGptvtXgmPeFiIHGufsvfH() {
        let loginVC = mbutnPfLndEDDkutxqFslcQGCcezBuuD.dJLzEXVoeuZIiampXDUjbJkzYaiYUSEL(identifierName: "LoginViewController")
        let viewModel = zNpuYXdnUpCUGKJbLuSAuuRuSZKlXbCr.init()
        loginVC.viewModel = viewModel
        viewModel.loginCoordinator = self
        navigationController.pushViewController(loginVC, animated: true)
    }
    
    func PKlHpzdCgeCVuOMjGnPPNsbEGkKmgSCO() {
        let coordinator = qlprgckGJhvcfJPVWskMBdFWoTvvLYmU.init(navigationController: navigationController)
        coordinator.parentCoordinator = self
        childCoordinators.append(coordinator)
        coordinator.ENLrvGDdGqBGptvtXgmPeFiIHGufsvfH()
    }
    
    func BvqwsGexBsqsaAODShQEpIXMkzQXIccF() {
        let coordinator = sYJJdgemLjmtfQAWHijdikEmeTmDKqIx.init(navigationController: navigationController)
        coordinator.parentCoordinator = self
        childCoordinators.append(coordinator)
        coordinator.ENLrvGDdGqBGptvtXgmPeFiIHGufsvfH()
    }
    
    func RFZxAFnsawgsRCPWxinttxNWiAzUDrjK(withViewModel: alPIGqhoWYQQOSEnbpRYEpqlSBSKnyCY? = nil) {
        var coordinator: KgZsElLXwiKXHeMFnLOchsliNaQvGwnL
        if let vwModel = withViewModel {
            coordinator = KgZsElLXwiKXHeMFnLOchsliNaQvGwnL.init(model: vwModel, navigationController: navigationController)
        } else {
            coordinator = KgZsElLXwiKXHeMFnLOchsliNaQvGwnL.init(navigationController: navigationController)
        }
        coordinator.parentCoordinator = self
        childCoordinators.append(coordinator)
        coordinator.ENLrvGDdGqBGptvtXgmPeFiIHGufsvfH()
    }
    
    func IaLIQTObiPuvWXpnQasnWhBdZjnvgolV(_ child: edtcilqUaIhCpEmLaXuQwDmcnYvTmQoB?, didSuccess: (() -> Void)? = nil) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                if let success = didSuccess {
                    success()
                }
                break
            }
        }
    }
}
