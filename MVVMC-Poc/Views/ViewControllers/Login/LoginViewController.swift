//
//  ViewController.swift
//  MVVMC-Poc
//
//  Created by SUNNY on 09/04/20.
//  Copyright © 2020 SUNNY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class mbutnPfLndEDDkutxqFslcQGCcezBuuD: UIViewController, rnCAWUnHLFrDJJnnrHYtHmqlBYgJKHau, klPqLpjYfxUsBCpOqGCYYiyDNcuBNiUn {
    
    @IBOutlet weak var txtFieldUserID: UITextField!
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    var viewModel: zNpuYXdnUpCUGKJbLuSAuuRuSZKlXbCr!

    override func viewDidLoad() {
        super.viewDidLoad()
        JsqJnCZIqAjDqXDUpbDBVUqetrxUrvYS(with: viewModel)
        // Do any additional setup after loading the view.
    }
    @IBAction func JNJZzFFAFQIOjMfckRdACKfCpaDbbwYv(_ sender: Any) {
        viewModel.BvqwsGexBsqsaAODShQEpIXMkzQXIccF()
    }
    
    @IBAction func YkzsuCzwgAKbTokncLooPduNQHaFMxGh(_ sender: Any) {
        viewModel.aRlBeMxJebptxnRjQECkBNKFoBOKFweW()
    }
    
    @IBAction func LDaWONEiXDRuYorZEwTTCIdNdqrSYCwq(_ sender: Any) {
        viewModel.TDAYfxFVhzylktnZeozlJoqodkngzrOg()
    }
}
extension mbutnPfLndEDDkutxqFslcQGCcezBuuD {
    private func JsqJnCZIqAjDqXDUpbDBVUqetrxUrvYS(with viewModel: zNpuYXdnUpCUGKJbLuSAuuRuSZKlXbCr){
        _ = txtFieldUserID.rx.text.map{ $0 ?? "" }.bind(to: viewModel.userIdText)
        _ = txtFieldPassword.rx.text.map{ $0 ?? "" }.bind(to: viewModel.passwordText)
        _ = viewModel.isLoginValid.bind(to: btnSignIn.rx.isEnabled)
        
        
//        viewModel.isLoadingAc
//            .observeOn(MainScheduler.instance)
//            .subscribe(onNext: { [unowned self] (isloading) in
//            if isloading {
//                self.showActivityIndicator()
//            } else {
//                self.hideActivityIndicator()
//            }
//            }, onCompleted: {
//                self.hideActivityIndicator()
//            }).disposed(by: viewModel.disposeBag)
        
        viewModel.isLoading
            .distinctUntilChanged()
            .drive(onNext: { [weak self] (isLoading) in
                guard let `self` = self else { return }
                self.OpTgdXryPrgHlOwPWqeTUFEtEzExSyAj()
                if isLoading {
                    self.ObUubaxsTHLTInMymcxPdBpIGcjHYNUu()
                }
            }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.loginDataResponse.subscribe(onNext: { (data) in
            if data.isSuccess {
                viewModel.xQdZsiPumikPeUIVBApjnfJCgiRakSmE()
            }
        }).disposed(by: viewModel.disposeBag)
    }
}
